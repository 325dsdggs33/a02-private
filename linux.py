# ======================== #
# ======================== #
# = Created By Anonymous = #
# ======================== #
# ======================== #

# ======================== #
def Logo_Awal():
    print("""
# ======================== #
# ======================== #
# = Created By Anonymous = #
# = Welcome Salam Santuy = #
# = Verison Bot : V.1.00 = #
# ======================== #
# ======================== #
    """, flush=True)
# ======================== #
Logo_Awal() # Logo Awal
# ======================== #

# ======================== #
from stem import Signal
from datetime import datetime
from pydub import AudioSegment
from selenium import webdriver
from stem.control import Controller
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, WebDriverException
# ======================== #
# pip3 install stem pydub SpeechRecognition selenium requests undetected-chromedriver==1.5.2
# ======================== #
import speech_recognition as sr
import undetected_chromedriver as uc
import random, urllib, os, sys, time, requests, urllib, pydub, re, json, stem.process
# ======================== #

# ======================== #
lantak = random.randint(1111111111,9999999999)
print(f"[!] System Alert! Kode Random : {lantak}")
# ======================== #
name = "Ayam Paha"
password = "LijayaAnli1188@"
domain = "roother.my.id"
vivaldi = "AkuNakal21"
clone = "https://shoukosagiri-poi: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@github.com/shoukosagiri-poi/A02-Private.git"
# ======================== #

# ======================== #
options = uc.ChromeOptions()
options.add_argument('--disable-notifications')
options.add_argument('--headless') # Linux/Windows
options.add_argument('--no-sandbox') # Linux/Windows
options.add_argument('--headless=chrome') # Linux/Windows
#options.add_argument(f'--proxy-server=socks5://127.0.0.1:9050') # Linux/Windows
options.add_argument('--window-size=1366,768')
options.add_extension('./ublock.crx')
driver = uc.Chrome(options=options)
# ======================== #

# ======================== #
def Captcha(): # Captcha Code
# ======================== #
    # Auto Locate Recaptcha Frames
    time.sleep(4)
    frames = driver.find_elements(By.TAG_NAME, 'iframe')
    recaptcha_control_frame = None
    recaptcha_challenge_frame = None
# ======================== #
    for index, frame in enumerate(frames):
        if re.search('reCAPTCHA', frame.get_attribute("title")):
            recaptcha_control_frame = frame
            print("[!] Captcha Alert! Sepertinya Ada Captcha")
        if re.search('recaptcha challenge', frame.get_attribute("title")):
            recaptcha_challenge_frame = frame
    if not (recaptcha_control_frame and recaptcha_challenge_frame):
        print("[!] Captcha Alert! Sepertinya Tidak Ada Captcha", flush=True)
        return # Lepas (def Captcha)
# ======================== #
    # Switch To Control 01 Recaptcha Frame
    time.sleep(4)
    driver.switch_to.default_content()
    frames = driver.find_elements(By.TAG_NAME, 'iframe')
    driver.switch_to.frame(recaptcha_challenge_frame)
# ======================== #
    # Click On Audio Challenge
    try:
        time.sleep(4)
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.ID, 'recaptcha-audio-button'))))
    except Exception as e:
        print("[!] Captcha Alert! Langsung Checklist", flush=True)
        driver.switch_to.default_content()
        return # Lepas (def Captcha)
# ======================== #
    # Switch To Control 02 Recaptcha Frame
    time.sleep(4)
    driver.switch_to.default_content()
    frames = driver.find_elements(By.TAG_NAME, 'iframe')
    driver.switch_to.frame(recaptcha_challenge_frame)
# ======================== #
    # Get The MP3 Audio File
    src = driver.find_element(By.ID, "audio-source").get_attribute("src")
    print(f"[!] Captcha Alert! Audio SRC : {src}", flush=True)
    path_to_mp3 = os.path.normpath(os.path.join(os.getcwd(), "sample.mp3"))
    path_to_wav = os.path.normpath(os.path.join(os.getcwd(), "sample.wav"))
# ======================== #
    # Download The MP3 Audio File From The Source
    urllib.request.urlretrieve(src, path_to_mp3)
    time.sleep(4)
# ======================== #
    # Load Downloaded MP3 Audio File As .WAV
    sound = pydub.AudioSegment.from_mp3(path_to_mp3)
    sound.export(path_to_wav, format="wav")
    sample_audio = sr.AudioFile(path_to_wav)
# ======================== #
    # Translate Audio To Text
    r = sr.Recognizer()
    with sample_audio as source:
        audio = r.record(source)
    key = r.recognize_google(audio)
    print(f"[!] Captcha Alert! Passcode : {key}", flush=True)
# ======================== #
    # Key In Result & Submit
    your_input = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, "audio-response")))
    your_input.send_keys(key.lower())
    your_input.send_keys(Keys.ENTER)
    Multiple01() # Check Multiple
# ======================== #
def Multiple01(): # Check Multiple
    time.sleep(4)
    try:
        err = driver.find_elements(By.CLASS_NAME, 'rc-audiochallenge-error-message')[0]
        if err.text == "" or err.value_of_css_property('display') == 'none':
            print("[!] Captcha Alert! Multiple Solved Need!", flush=True)
            Multiple02() # Solved Problem
    except Exception as e:
        print("[!] Captcha Alert! Multiple Solved No Need!", flush=True)
#         driver.switch_to.default_content()
        time.sleep(4)
        return # Lepas (def Captcha & Multiple)
# ======================== #
def Multiple02(): # Solved Problem
    # Get The MP3 Audio File
    time.sleep(4)
    src = driver.find_element(By.ID, "audio-source").get_attribute("src")
    print(f"[!] Captcha Alert! Audio SRC : {src}", flush=True)
    path_to_mp3 = os.path.normpath(os.path.join(os.getcwd(), "sample.mp3"))
    path_to_wav = os.path.normpath(os.path.join(os.getcwd(), "sample.wav"))
# ======================== #
    # Download The MP3 Audio File From The Source
    urllib.request.urlretrieve(src, path_to_mp3)
    time.sleep(4)
# ======================== #
    # Load Downloaded MP3 Audio File As .WAV
    sound = pydub.AudioSegment.from_mp3(path_to_mp3)
    sound.export(path_to_wav, format="wav")
    sample_audio = sr.AudioFile(path_to_wav)
# ======================== #
    # Translate Audio To Text
    r = sr.Recognizer()
    with sample_audio as source:
        audio = r.record(source)
    key = r.recognize_google(audio)
    print(f"[!] Captcha Alert! Passcode : {key}", flush=True)
# ======================== #
    # Key In Result & Submit
    your_input = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, "audio-response")))
    your_input.send_keys(key.lower())
    your_input.send_keys(Keys.ENTER)
    Multiple01() # Check Multiple
# ======================== #

# ======================== #
def A00(): # Check Alamat IP
    time.sleep(2)
    driver.get('https://api.ipify.org/')
    time.sleep(2)
    ip_address = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.TAG_NAME, "body"))).text
    print(f"[!] System Alert! Alamat IP : {ip_address}", flush=True)
# ======================== #

# ======================== #
def A01(): # Daftar Bitbucket
    time.sleep(2)
    print(f"[!] System Alert! Kode Random : {lantak}", flush=True)
    time.sleep(2)
    print("[!] Tahap 1 Mendaftar Bitbucket Dimulai", flush=True)
    time.sleep(2)
    driver.get("https://www.atlassian.com/try/cloud/signup?bundle=jira-software&edition=free")
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '/html/body/main/div/div/div/div[2]/div[2]/div[2]/div/div[2]'))))
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '/html/body/main/div/div/div/div[2]/div[2]/div[2]/form/div[1]/div/div/input')))
    your_input.send_keys(f"{lantak}@{domain}")
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '/html/body/main/div/div/div/div[2]/div[2]/div[2]/form/div[2]/div/div/input')))
    your_input.send_keys(name)
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '/html/body/main/div/div/div/div[2]/div[2]/div[2]/form/button/span/span'))))
    time.sleep(2)
    print("[!] Tahap 1 Mendaftar Bitbucket Captcha", flush=True)
    time.sleep(2)
    Captcha() # Captcha Code
    time.sleep(2)
    print("[!] Tahap 1 Mendaftar Bitbucket Captcha Selesai", flush=True)
    time.sleep(2)
    try:
        WebDriverWait(driver, 120).until(EC.presence_of_element_located((By.XPATH, '//*[@id="shopping-cart"]/optanon-script/div/div/div/img')))
        print("[!] Tahap 1 Mendaftar Bitbucket Selesai", flush=True)
    except Exception as e:
        print("[!] Tahap 1 Mendaftar Bitbucket Gagal", flush=True)
        driver.quit()
# ======================== #

# ======================== #
def A02(): # Verifikasi Email Vivaldi
    time.sleep(2)
    print("[!] Tahap 2 Verifikasi Email Dimulai", flush=True)
    time.sleep(2)
    driver.get("https://email.swenson.my.id/")
    time.sleep(10)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//div[2]/div[2]/div[3]'))))
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="user"]')))
    your_input.send_keys(f"{lantak}")
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='domain']"))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//form[1]/div[2]/div[1]/div[2]/div[1]/a[1]"))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='create']"))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//main[1]/div[1]/div[1]/div[1]"))))
    time.sleep(2)
    WebDriverWait(driver, 50).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH, "/html/body/div[1]/div/div[2]/div/main/div[2]/div[2]/iframe")))
    time.sleep(2)
    text = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, "//a[normalize-space()='Verify your email']")))
    yora = text.get_attribute('href')
    time.sleep(2)
    driver.get(yora)
    time.sleep(2)
    driver.switch_to.default_content()
    time.sleep(2)
    print("[!] Tahap 2 Verifikasi Email Selesai", flush=True)
    time.sleep(2)

# ======================== #

# ======================== #
def A03(): # Penyelesaian Akun
    try:
        WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '/html/body/div/div/div/div/div[2]/p[1]')))
        driver.refresh()
        time.sleep(2)
        A03()
    except Exception as e:
        pass
    time.sleep(2)
    print("[!] Tahap 3 Penyelesaian Akun Dimulai", flush=True)
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="password"]')))
    your_input.send_keys(password, Keys.ENTER)
    time.sleep(2)
    Captcha() # Captcha Code
    time.sleep(2)
    driver.get("https://bitbucket.org/account/signin")
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="js-username-field"]')))
    your_input.send_keys(lantak, Keys.ENTER)
    time.sleep(2)
    try:
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div/div[2]/div/div[1]/div[2]/div[5]/a/span/span/span'))))
        time.sleep(2)
    except Exception as e:
        pass
    print("[!] Tahap 3 Penyelesaian Akun Selesai", flush=True)
    time.sleep(2)
# ======================== #

# ======================== #
def A04(): # Proses Import
    time.sleep(2)
    print("[!] Tahap 4 Proses Import Dimulai", flush=True)
    time.sleep(2)
    driver.get(f"https://bitbucket.org/repo/import?workspace={lantak}")
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="id_url"]')))
    your_input.send_keys(clone)
    time.sleep(2)
    your_input = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, "//input[@id='id_project_name']")))
    your_input.send_keys("Private")
    time.sleep(2)
    #driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//label[normalize-space()='Private repository']"))))
    #time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, "//button[normalize-space()='Import repository']"))))
    time.sleep(5)
    print("[!] Tahap 4 Proses Import Selesai", flush=True)
    time.sleep(2)
# ======================== #

# ======================== #
def A05(): # Pembuatan Workspace
    time.sleep(2)
    print("[!] Tahap 5 Pembuatan Workspace Dimulai", flush=True)
    time.sleep(2)
    driver.get("https://bitbucket.org/workspace/create/")
    time.sleep(2)
    name_workspace = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="name-uid1"]')))
    name_workspace.send_keys(lantak)
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="root"]/div[2]/div/div[2]/div/div[1]/div/form/div[2]/div[3]/label/span[1]/span'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="create-workspace-button"]/span/span'))))
    time.sleep(2)
    print("[!] Tahap 5 Pembuatan Workspace Selesai", flush=True)
    time.sleep(2)
# ======================== #

# ======================== #
def A06(): # CircleCI Login
    time.sleep(2)
    print("[!] Tahap 6 Login CircleCI Dimulai", flush=True)
    time.sleep(2)
    driver.get("https://circleci.com/auth/signup")
    time.sleep(2)
    isi_email = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="email-signup"]')))
    isi_email.send_keys(f"{lantak}@{domain}")
    time.sleep(2)
    isi_password = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="password-signup"]')))
    isi_password.send_keys(password, Keys.ENTER)
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="purpose"]'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/form/section[1]/div[2]/div[2]/div/div[1]'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="role"]'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/form/section[2]/div[2]/div[2]/div/div[1]'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="useCase"]'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/form/section[3]/div[2]/div[2]/div/div[1]'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="engSize"]'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/form/section[4]/div[2]/div[2]/div/div[1]'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/form/div/button'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/button'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="oauth-authorize"]/section/form/div/div/button[1]'))))
    time.sleep(2)
    print("[!] Tahap 6 Login CircleCI Selesai", flush=True)
    time.sleep(2)
# ======================== #

# ======================== #
def A07(): # Tahap CircleCI Setup Dashboard
    time.sleep(2)
    print("[!] Tahap 7 CircleCI Setup Dashboard Dimulai", flush=True)
    time.sleep(2)
    driver.get(f"https://app.circleci.com/projects/bitbucket/{lantak}/setup")
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[1]/div/div[2]/button'))))
    time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="a02-private-0"]'))))
    time.sleep(2)
    try:
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 15).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[3]'))))
        time.sleep(2)
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[2]'))))
        time.sleep(2)
        branch = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[2]/div[2]/div[1]/input')))
        branch.send_keys("main")
        time.sleep(2)
    except Exception as e:
        driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[2]'))))
        time.sleep(2)
        branch = WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="__next"]/div/div/div[2]/section[2]/div/div[2]/button[2]/div[2]/div[1]/input')))
        branch.send_keys("main")
        time.sleep(2)
    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 50).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="__next"]/div/div/div[2]/div[1]/button'))))
    time.sleep(2)
    print("[!] Tahap 7 CircleCI Setup Dashboard Selesai", flush=True)
    time.sleep(9)
# ======================== #

# ======================== #
A00() # Check Alamat IP
A01() # Daftar Bitbucket
A02() # Verifikasi Email Vivaldi
A03() # Penyelesaian Akun
A04() # Proses Import
A06() # Tahap Login CircleCI
A07() # Tahap CircleCI Setup Dashboard
# ======================== #

# ======================== #
index_main = 0
index_back = 0
while index_main < index_back :
    lantak += 1
    A05() # Pembuatan Workspace
    A04() # Proses Import
    A07() # Tahap CircleCI Setup Dashboard
    index_main += 1
    print(f"[!] Ulangan Ke-{index_main}", flush=True)
# ======================== #

# ======================== #
def Logo_Akhir():
    print("""
# ======================== #
# ======================== #
# = Created By Anonymous = #
# = Goodbye Salam Santuy = #
# = Verison Bot : V.1.00 = #
# ======================== #
# ======================== #
    """, flush=True)
# ======================== #
Logo_Akhir() # Logo Awal
# ======================== #

# ======================== #
driver.quit()
# ======================== #

# ======================== #
# ======================== #
# = Created By Anonymous = #
# ======================== #
# ======================== #
